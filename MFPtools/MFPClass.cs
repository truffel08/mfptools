﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Printing;
using System.Management;
using System.Net;
using System.Text.RegularExpressions;
using Microsoft.Win32;

namespace MFPtools
{
    public abstract class MFPClass
    {
        
        private static string regName = @"SYSTEM\CurrentControlSet\Control\Class\{6BDD1FC6-810F-11D0-BEC7-08002BE2092F}";
        private static Dictionary<string, KeyValuePair<string, string>> scInstDict = new Dictionary<string, KeyValuePair<string, string>>();
        //  #Поддерживаемые модели
        private static string[] modelname = {"M1522", "M1536", "M225", "M521"};
        //#Файлы inf драйвера сканера
        private static string[] infmodel = { "hppasc08.inf", "hppasc16.inf", "hppasc_lj225226.inf", "hppasc_lj521.inf" };
        //#PID VID  сканеров
        private static string[] pidvidmodel = { "vid_03f0&pid_4C17&IP_SCAN", "vid_03f0&pid_012a&IP_SCAN", "VID_03F0&PID_2d2a&IP_SCAN", "VID_03F0&PID_272a&IP_SCAN" };



        /// <summary>
        ///  Searching Name and HostAddress for Standart Win32_TCPIPPrinterPort from Windows
        /// </summary>
        /// <returns>KeyValuePair with Name and HostAddress</returns>
        static public Dictionary<string, string> GetWin32TCPIPPrinterPort()
        {
            Dictionary<string, string> prnDict = new Dictionary<string, string>();

            ConnectionOptions connOptions = new ConnectionOptions
            {
                EnablePrivileges = true
            };

            ManagementScope mgmtScope = new ManagementScope("root\\CIMV2", connOptions);
            mgmtScope.Connect();

            ObjectQuery prntQuery = new ObjectQuery("SELECT * FROM Win32_TCPIPPrinterPort");
            ManagementObjectSearcher prnSearcher = new ManagementObjectSearcher(mgmtScope, prntQuery);

            foreach (ManagementObject prn in prnSearcher.Get())
            {
                prnDict.Add(prn["Name"].ToString().ToLower(), prn["HostAddress"].ToString().ToLower());
            }



            return prnDict;
        }

        /// <summary>
        ///  Get model name from http MFP title
        /// </summary>
        /// <returns>Return  model name</returns>
        static public string GetMFPModel(string hostName)
        {
            try
            {
                WebClient prnWC = new WebClient();
                //\< title\b[^>]*\>\s*(?<Title>[\s\S]*?)\</title\>
                return Regex.Match(Regex.Match(prnWC.DownloadString("http://" + hostName), @"(?<=<title>)([\S\s]*?)(?=</title>)", RegexOptions.IgnoreCase).Value, @"\b[a-zA-Z]\d+[a-zA-Z]*\b", RegexOptions.IgnoreCase).Value;//Regex.Match(prnWC.DownloadString("http://" + hostName), @"(?<=<title>)(\b[a-zA-Z]\d+[a-zA-Z]*\b)(?=</title>)", RegexOptions.IgnoreCase).Value;
            }
            catch { return ""; }
        }

        /// <summary>
        ///  Get list of installed scanners
        ///  Registry::HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Class\{6BDD1FC6-810F-11D0-BEC7-08002BE2092F}
        /// </summary>
        /// <returns>Return  portID, model name, registry subkey name</returns>
        static public Dictionary<string, KeyValuePair<string, string>> GetScanPorts()
        {
            Dictionary<string, KeyValuePair<string, string>> scanDict = new Dictionary<string, KeyValuePair<string, string>>();
            RegistryKey rgSeacher;


            rgSeacher = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(regName);

            foreach (string i in rgSeacher.GetSubKeyNames())
            {
                if (i.Contains("Properties") || i.Contains("Configuration")) continue;
                scanDict.Add(rgSeacher.OpenSubKey(i).OpenSubKey("DeviceData").GetValue("PortID").ToString().ToLower(), new KeyValuePair<string, string>(rgSeacher.OpenSubKey(i).GetValue("FriendlyName").ToString().ToLower(), i));
            }
            rgSeacher.Close();

            return scanDict;

        }
        /// <summary>
        /// IP/Host resovler
        ///  
        /// </summary>
        /// <returns>Return ip/hostname by hostname/ip</returns>
        static public string Resolver(string iporhost)
        {  //\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}
            // ^(?:(?:25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d\d?)$.
            try
            {
                if (Regex.IsMatch(iporhost, @"\b(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\b"))
                {
                    return Dns.GetHostEntry(iporhost).HostName.Split('.')[0];
                }
               else if (Regex.IsMatch(iporhost, @"^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))*$"))
                {
                    return Dns.GetHostEntry(iporhost).AddressList[0].ToString();
                }
               else return "";
            }
            catch
            {
                return "";
            }
        }
        /// <summary>
        /// Rename scanner.
        ///  
        /// </summary>
        static public bool Rename(string rlvStr, KeyValuePair<string, string> scanKV)
        {
            bool isOk=false;
            string str= rlvStr;
            try
            {
                if (Regex.IsMatch(rlvStr, @"\b(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\b"))
                {
                    str = Resolver(rlvStr);
                }
                else if (Regex.IsMatch(rlvStr, @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                {
                    isOk = false;
                }

                if (Regex.IsMatch(str, @"^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))*$"))
                {
                    RegistryKey rgSeacher = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(regName);
                    rgSeacher.OpenSubKey(scanKV.Value, true).SetValue("FriendlyName", str);
                    rgSeacher.Close();
                    isOk = true;
                }
                return isOk;
            }
            catch
            {
                return false;
            }
               
            
        }
        /// <summary>
        /// Install scanner
        ///  
        /// </summary>
        static public bool InstallScan(string rlvStr)
        {
            string isx64 = "";
            KeyValuePair<string, string> scinfvidKV;
            if (scInstDict.Count < 1 || scInstDict is null )
            {
                scInstDict = new Dictionary<string, KeyValuePair<string, string>>();
                for (int i = 0; i < modelname.Length; i++)
                {
                    scInstDict.Add(modelname[i], new KeyValuePair<string, string>(infmodel[i], pidvidmodel[i]));
                }
            }

            try
            {

                if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432")))
                {
                    isx64 = "64";
                }

                string model = Regex.Match(GetMFPModel(rlvStr), @"[a-zA-Z]\d+", RegexOptions.IgnoreCase).Value;

                scInstDict.TryGetValue(model, out scinfvidKV);
                string spath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                string s1 = @".\" + model + @"\scan" + isx64 + ".exe";
                string s2 = " -f " + scinfvidKV.Key + " -m " + scinfvidKV.Value + " -a " + rlvStr + " -n 1";
                System.Diagnostics.Process.Start(@".\" + model + @"\scan" + isx64 + ".exe", " -f " + scinfvidKV.Key + " -m " + scinfvidKV.Value + " -a " + rlvStr + " -n 1");
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// If scanner install, rename scanner by hostname
        /// Else install scanner
        /// </summary>
        static public bool InstallRename(Dictionary<string, string> prnDict, Dictionary<string, KeyValuePair<string, string>> scanDict)
        {

            string rlvStr;
            KeyValuePair<string, string> scanKV;
            bool isOk = false;

            foreach (KeyValuePair<string, string> prnKV in prnDict)
            {
                rlvStr = Resolver(prnKV.Value);

                if (scanDict.TryGetValue(prnKV.Value, out scanKV) || scanDict.TryGetValue(rlvStr, out scanKV)) //Если скан установлен
                {
                    if (!rlvStr.Equals("") && !rlvStr.Equals(scanKV.Key)) { isOk = Rename(rlvStr, scanKV); }//Переименовываем по небоходимости
                }
                else
                {
                    isOk = InstallScan(rlvStr);
                }
            }
            return isOk;
        }
    }
}
