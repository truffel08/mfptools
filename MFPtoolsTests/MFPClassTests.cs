﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MFPtools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MFPtools.Tests
{
    [TestClass()]
    public class MFPClassTests
    {
        [TestMethod()]
        /// <summary>Реальное имя хоста не МФУ</summary>
        public void GetMFPModelTest_realhost_notMFP()
        {
            string somereal = "localhost";

            Assert.AreEqual("", MFPClass.GetMFPModel(somereal));
        }
        [TestMethod()]
        /// <summary>Реальное имя хоста МФУ</summary>
        public void GetMFPModelTest_realhost_MFP()
        {
            string somereal = "kzn-prn4551m";

            Assert.AreEqual("M1536dnf", MFPClass.GetMFPModel(somereal));
        }
        [TestMethod()]
        /// <summary>Не существующее имя хоста</summary>
        public void GetMFPModelTest_not_realhost()
        {
            string somereal = "barbaris2000";

            Assert.AreEqual("", MFPClass.GetMFPModel(somereal));
        }
        [TestMethod()]
        /// <summary>Имя хоста больше стандарста RFC 2396 Section 3.2.2</summary>
        public void GetMFPModelTest_not_RFChost()
        {
            string somereal = "localhost";
            while (somereal.Length < 255) somereal += "localhost";

            Assert.AreEqual("", MFPClass.GetMFPModel(somereal));
        }
        [TestMethod()]
        /// <summary>Реальный ip не МФУ</summary>
        public void GetMFPModelTest_realip_notMFP()
        {
            string somereal = "127.0.0.1";

            Assert.AreEqual("", MFPClass.GetMFPModel(somereal));
        }
        [TestMethod()]
        /// <summary>Реальный ip МФУ</summary>
        public void GetMFPModelTest_realip_MFP()
        {
            string somereal = "10.192.11.34";

            Assert.AreEqual("M1536dnf", MFPClass.GetMFPModel(somereal));
        }
        [TestMethod()]
        /// <summary>Не верный ip</summary>
        public void GetMFPModelTest_badip_MFP()
        {
            string somereal = "10.192.11.266";

            Assert.AreEqual("", MFPClass.GetMFPModel(somereal));
        }

        /// <summary>
        /// Существующий хост не МФУ
        /// </summary>

        [TestMethod()]
        public void ResolverTest_realhost_notMFP()
        {
            string somereal = "kzn-4251m"; //::1 not work with ipv6

            Assert.AreEqual("10.192.10.95", MFPClass.Resolver(somereal));
        }
        /// <summary>
        /// Существующий хост МФУ
        /// </summary>

        [TestMethod()]
        public void ResolverTest_realhost_MFP()
        {
            string somereal = "kzn-prn4551m";//::1

            Assert.AreEqual("10.192.11.34", MFPClass.Resolver(somereal).ToLower());
        }
        /// <summary>
        /// Несуществующий хост 
        /// </summary>
        [TestMethod()]
        public void ResolverTest_notrealhost()
        {
            string somereal = "barbaris2000";//::1

            Assert.AreEqual("", MFPClass.Resolver(somereal));
        }

        /// <summary>
        /// Длинное имя хоста 
        /// </summary>
        [TestMethod()]
        public void ResolverTest_notRFCrealhost()
        {
            string somereal = "localhost";
            while (somereal.Length < 255) somereal += "localhost";

            Assert.AreEqual("", MFPClass.Resolver(somereal));
        }
        
        /// <summary>
        /// Существующий ip не МФУ
        /// </summary>
        [TestMethod()]
        public void ResolverTest_realip_notMFP()
        {
            string somereal = "10.192.10.95";

            Assert.AreEqual("kzn-4251m", MFPClass.Resolver(somereal).ToLower());
        }
        /// <summary>
        /// Существующий ip МФУ
        /// </summary>
        [TestMethod()]
        public void ResolverTest_realip_MFP()
        {
            string somereal = "10.192.11.34";

            Assert.AreEqual("kzn-prn4551m", MFPClass.Resolver(somereal).ToLower());
        }
        /// <summary>
        /// Несуществующий ip
        /// </summary>

        [TestMethod()]
        public void ResolverTest_not_realip()
        {
            string somereal = "10.192.11.266";
            MFPClass.Resolver(somereal); Assert.AreEqual("", MFPClass.Resolver(somereal).ToLower());
        }

        /// <summary>
        /// Несуществующий ip
        /// </summary>

        [TestMethod()]
        public void RenameTest_notexisting_subkey()
        {

            Assert.AreEqual(false, MFPClass.Rename("test", new KeyValuePair<string, string>("1", "")));
        }

        /// <summary>
        /// Несуществующий ключ реестра
        /// </summary>
        [TestMethod()]
        public void RenameTest_existing_subkey()
        {

            Assert.AreEqual(true, MFPClass.Rename("kzn-prn4551m", new KeyValuePair<string, string>("1", "0000")));
        }

        /// <summary>
        ///  Длинное имя
        /// </summary>

        [TestMethod()]
        public void RenameTest_longname()
        {
            string somereal = "localhost";
            while (somereal.Length < 255) somereal += "localhost";
            Assert.AreEqual(false, MFPClass.Rename(somereal, new KeyValuePair<string, string>("1", "")));
        }


        /// <summary>
        ///  IP
        /// </summary>

        [TestMethod()]
        public void RenameTest_digit_ip()
        {
            string somereal = "10.10.10.10";
            Assert.AreEqual(false, MFPClass.Rename(somereal, new KeyValuePair<string, string>("1", "")));
        }

        /// <summary>
        ///  Число похожее на IP
        /// </summary>

        [TestMethod()]
        public void RenameTest_digit_likeip()
        {
            string somereal = "999.999.999.999";
            Assert.AreEqual(false, MFPClass.Rename(somereal, new KeyValuePair<string, string>("1", "")));
        }


        /// <summary>
        /// Существующий хост МФУ
        /// </summary>
        [TestMethod()]
        public void InstallScanTest_realhostMFP()
        {
            Assert.AreEqual(true, MFPClass.InstallScan("kzn-prn4551m"));
        }

        /// <summary>
        /// Существующий ip МФУ
        /// </summary>
        [TestMethod()]
        public void InstallScanTest_realipMFP()
        {
            Assert.AreEqual(true, MFPClass.InstallScan("10.192.11.34"));
        }

        /// <summary>
        /// Существующий хост не МФУ
        /// </summary>
        [TestMethod()]
        public void InstallScanTest_realhostnotMFP()
        {
            Assert.AreEqual(false, MFPClass.InstallScan("localhost"));
        }

        /// <summary>
        /// Несуществующий хост 
        /// </summary>
        [TestMethod()]
        public void InstallScanTest_notrealhost()
        {
            Assert.AreEqual(false, MFPClass.InstallScan("barbaris2000"));
        }
        /// <summary>
        /// Несуществующий хост 
        /// </summary>
        [TestMethod()]
        public void InstallScanTest_notrealip()
        {
            Assert.AreEqual(false, MFPClass.InstallScan("10.192.11.266"));
        }

        /// <summary>
        /// Существующий ip не МФУ
        /// </summary>
        [TestMethod()]
        public void InstallScanTest_realipnotMFP()
        {
            Assert.AreEqual(false, MFPClass.InstallScan("10.192.0.25"));
        }

        /// <summary>
        /// МФУ уже есть в системе
        /// </summary>
        [TestMethod()]
        public void InstallRenameTest_installed()
        {
            Dictionary<string, string> prnDict = new Dictionary<string, string>();
            Dictionary<string, KeyValuePair<string, string>> scanDict = MFPClass.GetScanPorts();
            if (scanDict.Count < 1) throw new NotImplementedException();
            prnDict.Add(scanDict.First().Value.Key, scanDict.First().Value.Key);
            Assert.AreEqual(true, MFPClass.InstallRename(prnDict, scanDict));
        }

    

        /// <summary>
        /// Нет такого МФУ
        /// </summary>
        [TestMethod()]
        public void InstallRenameTest_notexist()
        {
            Dictionary<string, string> prnDict = new Dictionary<string, string>();
            Dictionary<string, KeyValuePair<string, string>> scanDict = MFPClass.GetScanPorts();
            if (scanDict.Count < 1) throw new NotImplementedException();
            prnDict.Add("kzn_prn4551m", "kzn-4551m");
            Assert.AreEqual(false, MFPClass.InstallRename(prnDict, scanDict));
        }

        /// <summary>
        /// Проверка на генерацию исключений
        /// </summary>
        [TestMethod()]
        public void GetWin32TCPIPPrinterPortTest_existtcpports()
        {
            if (MFPClass.GetWin32TCPIPPrinterPort() is null)
            {
                Assert.IsNull(MFPClass.GetWin32TCPIPPrinterPort());
            }
            else
            {
                Assert.IsNotNull(MFPClass.GetWin32TCPIPPrinterPort());
            }
        }

        /// <summary>
        /// Проверка на генерацию исключений
        /// </summary>
        [TestMethod()]
        public void GetScanPortsTest_notexistscan()
        {
            if (MFPClass.GetScanPorts() is null)
            {
                Assert.IsNull(MFPClass.GetScanPorts());
            }
            else
            {
                Assert.IsNotNull(MFPClass.GetScanPorts());
            }
        }
    }
}