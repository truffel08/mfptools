﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MFPtools;

namespace MFPInstall
{
    public partial class Form1 : Form
    {
        private MFPStorage mFp;
        public Form1()
        {
            InitializeComponent();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            listBoxPntPort.Items.Clear();
            foreach (KeyValuePair<string, string> i in MFPClass.GetWin32TCPIPPrinterPort())
            {
                listBoxPntPort.BeginUpdate();
                listBoxPntPort.Items.Add(i.Key+" is "+i.Value);
                listBoxPntPort.EndUpdate();

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBoxScanPort.BeginUpdate();
            listBoxScanPort.Items.Clear();
            foreach (KeyValuePair<string, KeyValuePair<string, string>> i in MFPClass.GetScanPorts())
            {
                             
                listBoxScanPort.Items.Add(i.Key + " is " + i.Value.Key);
                
            }
            if(MFPClass.GetScanPorts() is null)
            {
                listBoxScanPort.Items.Add("В системе не обнаружены сканеры");
            }
            listBoxScanPort.EndUpdate();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MFPClass.InstallRename(mFp.PrnDict,mFp.ScanDict);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            mFp = new MFPStorage();
        }

   
    }
}
