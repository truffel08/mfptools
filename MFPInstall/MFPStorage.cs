﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MFPtools;

namespace MFPInstall
{
    public
    sealed class MFPStorage
    {
        private Dictionary<string, string> prnDict;
        private Dictionary<string, KeyValuePair<string, string>> scanDict;
        public MFPStorage()
        {
             prnDict = MFPClass.GetWin32TCPIPPrinterPort();
            scanDict = MFPClass.GetScanPorts();
        }
        public Dictionary<string, string> PrnDict
        { get { return prnDict; } }
        public Dictionary<string, KeyValuePair<string, string>> ScanDict
        { get { return scanDict; } }
    }
}
